using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NultiTh240221
{
    class Program
    {
        static void Main(string[] args)
        {
            Task t1 = Task.Run(() =>
            {
                Thread.Sleep(1000);
                Console.WriteLine("Hello from task run");
            });
            t1.Wait();

            Task t2 = new Task(() =>
            {
                Thread.Sleep(1000);
                Console.WriteLine("Hello from new task");
            });
            t2.RunSynchronously(); // this is opposite from Async 

            Task t3 = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(1000);
                Console.WriteLine("Hello from Task.Factory");
            }, TaskCreationOptions.LongRunning); // not from thread pool
            t3.Wait();

            // ThreadPool.QueueUserWorkItem // summons a thread from the thread pool

            Task t4 = Task.Run(() =>
            {
                Console.WriteLine("Starting runner.....");
                Thread.Sleep(1000);
                Console.WriteLine("Hello from Task runner.........");
                Thread.Sleep(1000);
                Console.WriteLine("Done!!");
            });

            while (!t4.IsCompleted)
            {
                Thread.Sleep(10);
                Console.Write(".");
            }

            Task<int> t5 = Task.Run<int>(() =>
               {
                   return 5;
               });

            Task<string> t6 =  Task.Run<string>(() =>
            {
                Console.WriteLine("Task 6....");
                Thread.Sleep(3000);
                return "Hello world!";
            });

            string result_from_t6 = t6.Result; // blocking (=Wait)


        }
    }
}
