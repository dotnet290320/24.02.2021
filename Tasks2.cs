using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NultiTh240221
{
    class Program
    {
        static void Main(string[] args)
        {
            Task<int> t7 = Task.Run<int>(() =>
            {
                Console.WriteLine($"ID: {Task.CurrentId} will return 1");
                Thread.Sleep(1000);
                return 1;
            }).ContinueWith((Task<int> antecendent) =>
            {
                Console.WriteLine($"ID: {Task.CurrentId} will return 2");
                Thread.Sleep(1000);
                //antecendent.Status
                return antecendent.Result + 1;
            }).ContinueWith((Task<int> antecendent) =>
            {
                Console.WriteLine($"ID: {Task.CurrentId} will return 3");
                Thread.Sleep(1000);
                return antecendent.Result + 1;
            });

            Console.WriteLine(t7.Result);

            Task<int> t8_will_return_8 = Task.Run<int>(() => 8);
            Console.WriteLine(t8_will_return_8.Result);

            Task<int> t9_will_return_9 = Task.FromResult<int>(9);
            Console.WriteLine(t9_will_return_9.Result);

            Task<double> t10_will_return_10 = Task.FromResult<double>(
                Math.Pow(2, 10));
            Console.WriteLine(t10_will_return_10.Result);

            //object state = 1;
            //Task task_param = new Task((object o) => { Console.WriteLine(o); }, state);
            //task_param.Start();

            Task task_failure = Task.Run(() =>
            {
                Console.WriteLine("divide by zero task");
                int a = 5;
                int b = 0;
                int c = a / b;
                Console.WriteLine("code will not be reached. task faulted.");
            }).ContinueWith((Task papa) =>
            {
                Console.WriteLine("continue with after failure");
                Console.WriteLine(papa.Status);
                Console.WriteLine(papa.IsFaulted);
            });
            task_failure.Wait();
            Console.WriteLine("After...");
            Console.WriteLine(task_failure.Status);
            Console.WriteLine(task_failure.IsFaulted);

            Task task_failure2 = Task.Run(() =>
            {
                Console.WriteLine("divide by zero task");
                int a = 5;
                int b = 0;
                //int c = a / b; // wil crash the program
            });
            task_failure2.Wait();
        }
    }
}
