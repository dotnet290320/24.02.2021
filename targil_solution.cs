using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NultiTh240221
{
    class Program
    {
        static void Main(string[] args)
        {
            // tagril:
            // 1 use Stopwatch
            //   create 10 threads that print numbers from 1 to 10 , and run them
            //   how long did it take?
            //   create 10 Tasks that print numbers from 1 to 10 , and run them
            //   how long did it take?

            List<Thread> ten_threads = new List<Thread>();
            int max = 100;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < max; i++)
            {
                Thread t = new Thread(() =>
                {
                    for (int j = 1; j <= 10; j++)
                    {
                        Console.Write(j);
                    }
                    Console.WriteLine();
                });
                t.Start();
                ten_threads.Add(t);
            }
            foreach (var item in ten_threads)
            {
                item.Join();
            }

            var time = sw.ElapsedMilliseconds;
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine($"Took {time} ms"); // ~394
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            sw.Stop();

            List<Task> ten_tasks = new List<Task>();
            sw.Reset();
            sw.Start();
            for (int i = 0; i < max; i++)
            {
                Task t = Task.Run(() =>
                {
                    for (int j = 1; j <= 10; j++)
                    {
                        Console.Write(j);
                    }
                    Console.WriteLine();
                });
                ten_tasks.Add(t);
            }
            Task.WaitAll(ten_tasks.ToArray());

            time = sw.ElapsedMilliseconds;
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine($"Took {time} ms"); // ~
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            sw.Stop();

            // 2 do the same like 1 but this time use threads from thread pool 
            //   ThreadPool.QueueUserWorkItem
            List<Thread> ten_threads_pool = new List<Thread>();

            sw = new Stopwatch();
            sw.Start();
            int counter = 0;
            for (int i = 0; i < max; i++)
            {
                ThreadPool.QueueUserWorkItem((o) =>
                {
                    for (int j = 1; j <= 10; j++)
                    {
                        Console.Write(j);
                    }
                    Console.WriteLine();
                    counter++;
                }, null);
            }
            while (counter < max)
            {
                Thread.Sleep(10);
            }
            time = sw.ElapsedMilliseconds;
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine($"Took {time} ms"); // ~
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            Console.WriteLine(" =========================================== ");
            sw.Stop();

            // 3 readline 2 numbers from the user
            //   create a task that computes the sum of this two numbers
            //   have the task use a return value 
            //   get the result value using : <task-name>.Result
            //   print the result
            Console.WriteLine("Insert number #1");
            int num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Insert number #2");
            int num2 = int.Parse(Console.ReadLine());
            Task<int> sum_task = new Task<int>(() =>
            {
                Console.WriteLine("num1 + num2");
                Thread.Sleep(1000);
                return num1 + num2;
            });
            sum_task.Start();
            Console.WriteLine($"{num1} + {num2} = {sum_task.Result}");

            // 4 do the same as 3 but this time use Task.FromResult
            Console.WriteLine("Insert number #3");
            int num3 = int.Parse(Console.ReadLine());
            Console.WriteLine("Insert number #4");
            int num4 = int.Parse(Console.ReadLine());
            Task<int> sum_task2 = Task.FromResult(num3 + num4);
            Console.WriteLine($"{num3} + {num4} = {sum_task2.Result}");

            // 5 create tasks that computes 2 power 4 ,
            //   each task will make a power 2 and descent to the next task
            //   collect the result value
            Task<double> power_2 = Task<double>.Run(() =>
            {
                return 2.0; // 2 * 1
            }).ContinueWith((Task<double> papa) =>
            {
                return papa.Result * 2; // 2 * 2
            }).ContinueWith((Task<double> papa) =>
            {
                return papa.Result * 2; // 2 * 2 * 2
            }).ContinueWith((Task<double> papa) =>
            {
                return papa.Result * 2; // 2 * 2 * 2 * 2
            });
            Console.WriteLine($"2 power 4 = {power_2.Result}");

        }
    }
}
